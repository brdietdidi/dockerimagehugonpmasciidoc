# syntax=docker/dockerfile:1
# start with a raw ubuntu distribution
FROM klakegg/hugo:0.92.1-asciidoctor-ci

ENV NODE_VERSION=14.19.0
ENV ASCIIDOCTOR_VERSION=2.0.15
ENV HUGO_VERSION=0.92.1

# install npm and nodejs
RUN apk add --update nodejs npm

# collect the versions of the installed stuff
RUN cat /etc/os-release > versions.txt
RUN { echo "node version: "; node --version; } >> versions.txt
RUN asciidoctor --version >> versions.txt
RUN hugo version >> versions.txt

EXPOSE 1313

# print the versions of the installed stuff
CMD cat versions.txt
